// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TDS_Project.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDS_Project, "TDS_Project" );

DEFINE_LOG_CATEGORY(LogTDS_Project)
 